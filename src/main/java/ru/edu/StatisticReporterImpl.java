package ru.edu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.io.FileNotFoundException;
import java.io.IOException;


public class StatisticReporterImpl implements StatisticReporter {


    /**
     * Файл.
     */
    private File file;


    /**
     * Путь файла.
     *
     * @param filePath
     */
    public StatisticReporterImpl(final String filePath) {
        file = new File(filePath);

    }


    /**
     * Формирование отчета.
     *
     * @param statistics - данные статистики
     */
    @Override
    public void report(final TextStatistics statistics) {
        try (FileOutputStream os = new FileOutputStream(file);
             OutputStreamWriter sw = new OutputStreamWriter(os,
                     StandardCharsets.UTF_8);
             PrintWriter writer = new PrintWriter(sw)) {
            writer.println("Слов: " + statistics.getWordsCount());
            writer.println(("Символов: "
                    + statistics.getCharsCount()));
            writer.println("Без пробелов: "
                    + statistics.getCharsCountWithoutSpaces());
            writer.println("Символов пунктуации: "
                    + statistics.getCharsCountOnlyPunctuations());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
