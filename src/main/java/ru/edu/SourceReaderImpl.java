package ru.edu;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

public class SourceReaderImpl implements SourceReader {

    /**
     * Файл для чтения.
     */
    private File file;

    /**
     * Установка источника.
     * В реализации тут будет приходить путь до файла-источника
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException("Source incorrect");
        }

        file = new File(("./src/test/resources/input_text.txt"));
        if (!file.exists()) {
            throw new IllegalArgumentException("File is missing");
        }


    }

    /**
     * Метод для анализа источника.
     *
     * @param analyzer - логика подсчета статистики
     * @return - рассчитанная статистика
     */
    @Override
    public TextStatistics readSource(final TextAnalyzer analyzer) {

        if (analyzer == null) {
            throw new IllegalArgumentException(" Analyzer "
                    + "is Null");
        }

        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {


            processFile(analyzer, br);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return analyzer.getStatistic();

    }

    private void processFile(final TextAnalyzer analyzer,
                             final BufferedReader br)
            throws IOException {
        String line;
        boolean firstLine = true;
        while ((line = br.readLine()) != null) {
            analyzer.analyze(!firstLine ? "\n" + line : line);
            firstLine = false;
        }
        br.readLine();


    }
}
