package ru.edu;

import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

public class SourceReaderImplTest {

    private final static String INPUT_TEXT1 = "./src/test/resources/testFile.txt";
    private SourceReader reader = new SourceReaderImpl();


    @Test
    public void setupTextFile() {
        reader.setup(INPUT_TEXT1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNull(){
        reader.setup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNotExistFile(){
        reader.setup(null);
    }

    @Test
    public void readSource() {
        TestTextAnalyzer analyzer = new TestTextAnalyzer();
        reader.setup(INPUT_TEXT1);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);
        assertEquals(514, statistics.getWordsCount());

        assertEquals("Идея рассказа \"Ветер из Геттисберга\" впервые  появилась  у  меня  после", analyzer.lines.get(0));



    }

    static class TestTextAnalyzer implements TextAnalyzer {

        public List<String> lines = new ArrayList<>();

        /**
         * Анализ строки произведения.
         *
         * @param line
         */
        @Override
        public void analyze(String line) {
            lines.add(line);

        }

        /**
         * Получение сохраненной статистики.
         *
         * @return TextStatistic
         */
        @Override
        public TextStatistics getStatistic() {
            TextStatistics statistics = new TextStatistics();
            statistics.addWordsCount(lines.size());
            return statistics;
        }
    }
}