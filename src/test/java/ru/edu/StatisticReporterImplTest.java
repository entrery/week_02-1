package ru.edu;

import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class StatisticReporterImplTest {

    public static final String RESULT_TXT = "./target/result111.txt";

    @Test
    public void writeStats() {
        StatisticReporter reporter = new StatisticReporterImpl(RESULT_TXT);

        TextStatistics statistics = new TextStatistics();

        statistics.addCharsCount(100);
        statistics.addWordsCount(50);
        statistics.addCharsCountOnlyPunctuations(5);
        statistics.addCharsCountWithoutSpaces(10);

        reporter.report(statistics);
        assertEquals(4, readFile().size());
        assertEquals("Слов: 50", readFile().get(0));
        assertEquals("Символов: 100", readFile().get(1));
        assertEquals("Без пробелов: 10", readFile().get(2));
        assertEquals("Символов пунктуации: 5", readFile().get(3));


    }

    private List<String> readFile() {
        List<String> lines = new ArrayList<>();
        String resultTxt = RESULT_TXT;
        try (FileReader fr = new FileReader(resultTxt);
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

}