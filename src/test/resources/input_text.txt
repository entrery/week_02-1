Идея рассказа "Ветер из Геттисберга" впервые  появилась  у  меня  после
посещения  диснеевской  фабрики  по  изготовлению  игрушечных  роботов   в
Глендайле. Я смотрел, как собирают на конвейере механического Линкольна, и
вдруг представил себе убийцу Бутса и театр Форда в  тот  апрельский  вечер
1865 года... И я написал рассказ. Это  очень  "личное"  произведение.  Мой
герой  во  многом  передает  мысли,  переживания  и  смятение,  которые  я
испытывал после покушений на Мартина Лютера Кинга и Роберта Кеннеди.


   В 10:15 вечера  он  услышал  резкий,  похожий  на  выстрел  звук,  эхом
отдавшийся по театральным помещениям.
   "Выхлоп газа, - подумал он. - Нет. Выстрел".
   Секундой позже он услышал взрыв людских голосов, и  затем  все  стихло,
как затихает океанская волна, удивленно накатываясь на  пологий  берег.  С
шумом хлопнула дверь. Топот бегущих ног.
   Бледный,  как  смерть,  в  комнату  ворвался  билетер,  словно  слепой,
скользнул вокруг невидящим взглядом, в смятении выдавил из себя:
   - Линкольн... Линкольн...
   Байес оторвался от стола:
   - Что с Линкольном?
   - В него... Он убит!
   - Весьма остроумно...
   - Убит. Понимаете? Убит. Действительно убит. Убит вторично!
   Билетер вышел, пошатываясь и держась за стену.
   Байес непроизвольно встал со ступа. "Ради бога, только не это..."
   Он побежал, обогнал билетера, который, чувствуя, что его обгоняют, тоже
побежал рядом с ним.
   "Нет, нет, - подумал Байес. - Только не это. Этого не  было.  Не  могло
быть. Не было, не могло быть".
   - Убит, - сказал билетер.
   Сразу за поворотом коридора с треском распахнулись театральные двери, и
толпа - кричащее, вопящее, ревущее, оглушающее, дикое сборище -  зашумела,
забурлила: "Где он?", "Там!", "Это он?", "Где?",  "Кто  стрелял?",  "Он?",
"Держи его!", "Берегись!", "Стоп!"
   Спотыкаясь, расталкивая толпу,  прокладывая  там  и  тут  себе  дорогу,
показались  два  охранника  и  между   ними   человек,   изворачивающийся,
пытающийся оторваться от вцепившихся рук,  увернуться  от  вздымающихся  и
падающих на него кулаков. Его хватали, щипали, били свертками  и  хрупкими
солнечными зонтиками, которые разлетались в щепки, как  воздушные  змеи  в
сильный  шторм.  Женщины  в  панике  закружились   по   фойе,   разыскивая
потерявшихся спутников. Мужчины с  криками  отталкивали  их  в  сторону  и
бросались в центр этого водоворота, туда, где охранники расталкивали толпу
и где человек,  стоявший  между  ними,  обхватил  руками  низко  опущенную
голову.
   "О боже) - Байес застыл от ужаса,  начиная  верить.  -  Боже  мой!"  Он
взглянул не сцену, лотом бросился вперед:
   - Сюда! Все назад! Освободите помещение! Сюда! Сюда!
   И толпа каким-то чудом разорвалась. С треском распахнулись  театральные
двери и потом захлопнулись, пропустив наружу разгоряченные тела.
   На улице толпа бурлила и клокотала, угрожая проклятиями и  неслыханными
карами. Весь театр сотрясался от бессвязных воплей, криков и  предсказаний
страшного суда.
   Байес долго глядел  на  трясущиеся  дверные  ручки,  дрожащие  замки  и
защелки, на охранников и человека, зажатого между ними.
   Внезапно он отскочил назад, как будто еще что-то,  еще  более  ужасное,
стряслось здесь, в проходе между рядами.  Его  левый  ботинок  ударился  о
какой-то предмет, который отлетел в сторону  и  закружился  на  ковре  под
креслами, как крыса, играющая со своим хвостом. Байес нагнулся  и  вслепую
нащупал под креслами теплый еще пистолет. Вернувшись обратно в проход,  он
сунул пистолет в  карман.  Прошло  не  меньше  минуты,  прежде  чем  Байес
заставил себя повернуться в сторону сцены и этой фигуры посередине.
   Авраам Линкольн  сидел  в  своем  резном  высоком  кресле,  его  голова
откинулась  в  сторону  и  повисла  в  неестественном  положении.   Широко
раскрытые глаза глядели в пустоту. Его  большие  руки  мягко  отдыхали  на
подлокотниках, как будто в любую минуту он мог податься вперед,  встать  и
объявить это грустное происшествие оконченным.
   С трудом переставляя ноги, как будто под проливным дождем, Байес  пошел
на сцену.
   - Свет, черт возьми! Дайте больше света!
   Где-то там, за сценой, невидимый  электрик  вспомнил  вдруг,  для  чего
существует рубильник. Подобие рассвета забрезжило в мрачном, темном  зале.
Байес поднялся на помост, обошел вокруг Линкольна и остановился.
   Да. Так и есть. Маленькое  аккуратное  пулевое  отверстие  в  основании
черепа за левым ухом.
   -  Sic  semper  tyrannis  [так  будет  со  всеми  тиранами  (лат.)],  -
пробормотал где-то незнакомый голос.
   Байес резко поднял голову.
   Убийца сидел теперь в последнем ряду театрального зала. Опустив  голову
вниз, он говорил в пол, как будто самому себе:
   - Sic...
   Он смолк на полуслове, почувствовав опасное движение над головой. Кулак
одного из охранников взлетел  вверх,  как  будто  человек  ничего  не  мог
поделать с собой. Кулак готов был уже опуститься на голову  убийцы,  чтобы
заставить его замолчать.
   - Не надо! - сказал Байес.
   Кулак замер в воздухе.  Охранник  отвел  руку  в  сторону,  в  гневе  и
отчаянии сжимая и разжимая пальцы.
   "Не было, - подумал Байес. - Ничего не  было.  Ни  этого  человека,  ни
охраны, ни..." Он повернулся и еще раз посмотрел  на  отверстие  в  голове
убитого президента.
   Из отверстия медленно капало машинное масло.
   Такое же масло стекало изо рта Линкольна по подбородку и бакенбардам  и
падало капля за каплей на галстук и рубашку.
   Байес встал на колени и приложил ухо к груди  Линкольна.  Там,  глубоко
внутри,  слабо  тикали  и  жужжали  шестеренки,  колесики,   пружины,   не
поврежденные, но работающие просто по инерции.
   По какой-то сложной ассоциации  этот  угасающий  звук  заставил  его  в
тревоге подняться на ноги.
   - Фиппс?! - пробормотал Байес.
   Охранники переглянулись в недоумении.
   Байес сжал руки:
   - Фиппс собирался прийти сегодня? Боже мой, он не должен видеть  этого!
Ступайте, позвоните ему, придумайте  что-нибудь.  Скажите,  что  произошла
авария, да, авария на заводе в Глендайле. Быстрее!
   Один из охранников выбежал из зала.
   "Боже, задержи его дома, пусть он не видит этого", - подумал Байес.
   Странно, в такую  минуту  он  думал  не  о  себе.  Жизнь  других  людей
замелькала перед глазами.
   Помнишь... тот день, пять лет назад, когда  Фиппс  небрежно  бросил  на
стол чертежи, эскизы, акварели и объявил о своем великом плане? И как  все
они уставились на рисунки, потом на него и выдохнули: "Линкольн?"
   Да! Фиппс рассмеялся, как отец, только что вернувшийся из  церкви,  где
некое высшее видение обещало ему необычайно одаренного ребенка.
   Линкольн. В этом что-то было. Линкольн, рожденный вновь.
   А  Фиппс?  Он  создаст  и  воспитает  этого  сказочного,  вечно  живого
гигантского ребенка-робота.
   Разве это не прекрасно...  стоять  среди  лугов  Геттисберга,  слушать,
учиться, смотреть, править лезвия наших бритвенных душ и жить?
   Байес ходил вокруг тяжело осевшей фигуры, поглощенный воспоминаниями.
   Фиппс,  поднявший  рюмку  над  головой,  как  линзу,  что  одновременно
собирает в фокусе лучи прошлого и освещает будущее.
   "Я всегда мечтал сделать такой фильм: "Геттисберг [1-3 июля  1863  года
возле небольшого  города  Геттисберга  произошло  крупное  сражение  между
войсками Юга и Севера,  которое  закончилось  победой  северян  и  явилось
поворотным пунктом в ходе всей Гражданской войны; вскоре после  битвы  при
Геттисберге была образована комиссия по  созданию  мемориального  кладбища
для захоронения 3814 американских солдат; организаторы послали приглашение
президенту Линкольну присутствовать на  торжественном  открытии  кладбища;
речь Линкольна вошла в историю ораторского искусства и историю Америки как
одна из самых ярких ее  страниц;  текст  "геттисбергской  речи"  выбит  на
граните Мемориального музея Линкольна в  Вашингтоне]  и  огромное  людское
море; и там, далеко на краю этой дремлющей на  солнце  беспокойной  толпы,
фермер с сыном, напряженно слушающие  и  ничего  не  слышащие,  пытающиеся
уловить разносимые ветром слова высокого оратора там, на далекой  трибуне.
Вот он снимает цилиндр, смотрит в него, как будто смотрит себе в  душу,  и
начинает говорить.
   И фермер сажает сына к себе  на  плечи,  чтобы  поднять  его  над  этой
сдавленной многотысячной толпой. Высокий голос  президента  разносится  по
округе то ясный и чистый, то слабый и отдаленный,  захваченный  в  плен  и
разносимый в стороны гуляющими над полем ветрами.
   Много ораторов выступало уже до него, и толпа устала,  превратившись  в
сплошной комок шерсти и пота. Фермер нетерпеливо шепчет сыну:
   - Ну что? Что он говорит?
   И мальчик, весь подавшись вперед и  повернув  по  ветру  пушистое,  как
персик, ухо, шепчет в ответ:
   - Восемьдесят семь лет...
   - Ну?
   - ...тому назад отцы наши основали...
   - Ну, ну?!
   - ...на этом континенте...
   - Ну?
   - ...новую нацию, рожденную свободной и вдохновленную  той  идеей,  что
все люди...
   И так это продолжалось: ветер, разносящий во все стороны хрупкие  слова
далекого оратора, фермер, позабывший про тяжкую ношу, и  сын,  приложивший
руки к ушам, схватывающий смысл речи, пропускающий иногда целые фразы,  но
все вместе замечательно понятное до самого конца:
   - ...правительство народа, избранное народом и для народа...
   - ...не исчезнет с лица земли.
   Мальчик замолчал.
   - Он кончил.
   И толпа разбрелась во все стороны. И Геттисберг вошел в историю".
   Байес сидел, не отрывая глаз от Фиппса.
   Фиппс выпил рюмку до дна,  внезапно  смутившись  своей  экспансивности,
потом бросил:
   - Я никогда не поставлю такой фильм. Но я сделаю ЭТО.
   Именно тогда он вытащил и разложил на столе свои рисунки  и  чертежи  -
Фиппс   Эверди   Салем,   Иллинойс   и   Спрингфилдский   призрак-автомат,
механический Линкольн, электро-масло-смазочная пластмассово-каучуковая, до
мелочей продуманная  сокровенная  мечта.  Возвращенный  к  жизни  чудесами
технологии,  возрожденный  романтиком,   вычерченный   отчаянной   нуждой,
говорящий голосом неизвестного актера, он будет жить  вечно  там,  в  этом
далеком юго-западном уголке Америки! Линкольн и Фиппс!
   Фиппс и его взрослый, двухметровый от рождения Линкольн. Линкольн!
   "Мы все должны стоять на ветру из Геттисберга. Только так  можно  будет
что-нибудь услышать".
   Он поделился  с  ними  своим  изобретением.  Одному  доверил  арматуру,
другому - скелет, третий должен был подобрать "линкольновский" голос и его
лучшие выступления. Остальные доставали драгоценную кожу,  волосы,  делали
отпечатки пальцев. Да, даже ПРИКОСНОВЕНИЕ Линкольна должно быть таким  же,
точно скопированным с оригинала!
   Все они жили тогда, посмеиваясь над собой. Эйб  никогда  не  сможет  на
самом деле ни говорить, ни двигаться, все прекрасно понимали это.
   Но по мере того как работа продолжалась и месяцы растягивались в  годы,
их насмешливо-иронические реплики уступали место одобрительным  улыбкам  и
дикому энтузиазму.
   Они   были   бандой   мальчишек,    вовлеченных    в    некое    тайное
воспаленно-счастливое  погребальное  общество,  встречавших  полночь   под
сводами мраморных склепов  и  разбегавшихся  на  рассвете  меж  надгробных
памятников.
   Бригада Воскресения Линкольна процветала.  Вместо  одного  сумасшедшего
десяток маньяков  кинулся  рыться  в  старых  запылившихся  и  пожелтевших
подшивках газет, выпрашивать посмертные маски,  делать  формы  и  отливать
пластмассовые кости.
   Некоторые отправились по местам боев Гражданской войны в  надежде,  что
история, рожденная на утренних ветрах, поднимет их плащи и  заколышет  их,
как флаги. Другие  бродили  по  октябрьским  полям  Салема,  загоревшие  в
последних лучах лета,  задыхаясь  от  свежего  воздуха,  навострив  уши  в
надежде уловить не записанный  на  пленки  и  пластинки  голос  худощавого
юриста.
   Но, конечно, никто из них не был столь одержим  и  не  испытывал  столь
сильно гордых мук отцовства, как Фиппс. Наконец наступил день, когда почти
готовый робот был разложен на монтажно-сборочных  столах;  соединенный  на
шарнирах, с вмонтированной системой подачи голоса,  с  резиновыми  веками,
закрывающими  глубоко  посаженные  грустные  глаза,  которые,   пристально
всматриваясь в мир, видели слишком многое. К голове приставили благородные
уши, которые могли слышать лишь время прошедшее. Большие руки с узловатыми
пальцами  были  подвешены,  как  маятники,   отсчитывающие   это   ушедшее
безвозвратно время. И когда все было  готово,  они  надели  костюм  поверх
нагой фигуры, застегнули пуговицы, затянули узел  на  галстуке  -  конклав
портных, нет, апостолов, собравшихся ярким славным пасхальным утром.
   В последний час последнего дня Фиппс выгнал их  всех  из  комнаты  и  в
одиночестве нанес последние мазки  великого  художника,  потом  позвал  их
снова и не буквально, нет, но в каком-то  метафорическом  смысле  попросил
вознести его на плечи.
   Притихшие, смотрели они, как Фиппс взывал над старым полем боя и далеко
за его пределами, убеждая, что могила не место для него: воскресни!
   И Линкольн, спавший глубоким сном  в  своем  прохладном  спрингфилдском
мраморном покое, повернулся и в сладком сновидении увидел себя ожившим.
   И встал.
   И заговорил.


   Зазвонил  телефон.  Байес  вздрогнул  от  неожиданности.   Воспоминания
рассеялись.
   - Байес? Это Фиппс. Бак звонил только что. Говорит, чтобы я  немедленно
ехал. Говорит, что-то с Линкольном...
   - Нет, - сказал Байес.  -  Ты  же  знаешь  Бака.  Наверняка  звонил  из
ближайшего бара. Я здесь, в театре. Все в  порядке.  Один  из  генераторов
забарахлил. Мы только что кончили ремонт...
   - Значит, с НИМ все в порядке?
   - Он велик, как всегда. - Байес не мог отвести глаз от тяжело  осевшего
тела.
   - Я... Я приеду...
   - Нет, не надо!
   - Бог мой, почему ты КРИЧИШЬ?
   Байес прикусил язык, сделал  глубокий  вдох,  закрыл  глаза,  чтобы  не
видеть этой фигуры в кресле, потом медленно сказал:
   - Фиппс, я не кричу. В зале только что дали свет. Публика  ждет,  я  не
могу сдержать их. Я клянусь тебе...
   - Ты лжешь.
   - Фиппс!
   Но Фиппс повесил трубку.
   "Десять минут, - в смятении подумал  Байес.  -  Он  будет  здесь  через
десять минут. Всего десять минут  -  и  человек,  вернувший  Линкольна  из
могилы, встретится с тем, кто вогнал его туда обратно".
   Он резко встал. Бешеная жажда деятельности овладела им.  Скорее  бежать
туда,  за  сцену,  включить  магнитофон,  посмотреть,  какие  узлы   можно
заменить, а что уже никогда  не  поправить.  Впрочем,  нет.  Нет  времени.
Оставим это на завтра.
   Время оставалось лишь для разгадки тайны.
   И тайна эта заключалась в человеке,  который  сидел  сейчас  в  третьем
кресле последнего ряда.
   Убийца - ведь он УБИЙЦА, не так ли? Что он представляет собой?
   Байес ведь видел совсем недавно это лицо.  До  боли  знакомое  лицо  со
старого поблекшего и позабытого уже  дагерротипа.  И  эти  пышные  усы.  И
темные надменные глаза.
   Байес  медленно  сошел  со  сцены.  Медленно  поднялся  по  проходу  до
последнего ряда и остановился, глядя  на  человека  с  опущенной  головой,
зажатой между руками.
   - Мистер... Бутс?
   Странный незнакомец сжался, потом вздрогнул и выдавил шепотом:
   - Да...
   Байес выждал. Потом собрался с силами и спросил:
   - Мистер... Джон Уилкес Бутс? [убийца президента Авраама Линкольна]
   Убийца тихо рассмеялся. Смех перешел в какое-то зловещее карканье.
   - Норман Левелин Бутс. Только фамилия... совпадает.
   "Слава богу, - подумал Байес. - Я бы не вынес этого".
   Он повернулся и пошел вдоль прохода, потом остановился  и  взглянул  на
часы. Стрелка неумолимо бежала вперед. Фиппс уже в дороге. В любой  момент
он может забарабанить в дверь.
   - Почему?..
   - Я не знаю! - крикнул Бутс.
   - Лжешь!
   - Удобная возможность. Жалко было упустить.
   - Что? - Байес резко повернулся.
   - Ничего.
   - Ты не посмеешь повторить это.
   - Потому что... - начал Бутс, опустив голову, -  ...потому  что...  это
правда, - прошептал он в благоговейном трепете. - Я сделал это... В  САМОМ
ДЕЛЕ сделал это.
   Стараясь как-то сдержаться, Байес продолжал  ходить  вверх  и  вниз  по
проходам, боясь остановиться, боясь, что нервы не выдержат и он бросится и
будет бить, бить, бить этого убийцу.
   Бутс заметил это.
   - Чего вы ждете? Кончайте...
   - Я не сделаю этого... - Байес заставил себя  успокоиться.  -  Меня  не
будут судить за убийство, за то, что я убил человека, который убил другого
человека, который, в сущности, и не был человеком, а  машиной.  Достаточно
уже того, что застрелили вещь только за то, что она выглядит как живая.  И
я не хочу ставить в  тупик  судью  или  жюри  и  заставлять  их  рыться  в
уголовном кодексе  в  поисках  подходящей  статьи  для  человека,  который
убивает потому,  что  застрелен  человекоподобный  компьютер.  Я  не  буду
повторять твоего идиотизма.
   - Жаль, - вздохнул человек, назвавшийся Бутсом. Краска медленно сошла с
его лица.
   - Говори, - сказал Байес,  уставившись  в  стену.  Он  представил  себе
ночные улицы, Фиппса, мчащегося в своей машине, неумолимо убегающее время.
- У тебя есть пять минут, может, больше, может, меньше. Почему  ты  сделал
это, почему? Начни с чего-нибудь. Начни с того, что ты трус.
   - Трус, да, - сказал Бутс. - Откуда вы знаете?
   - Я знаю.
   - Трус, - повторил Бутс. - Это точно. Это я. Всегда боюсь. Вы правильно
назвали. Всего боюсь. Вещей. Людей. Новых мест. Боюсь. Людей, которых  мне
хотелось ударить и которых я никогда не тронул  пальцем.  Вещей  -  всегда
хотел их иметь - никогда не было. Мест, куда хотел поехать и  где  никогда
не бывал. Всегда хотел быть большим, знаменитым. Почему бы и нет? Тоже  не
получилось. Так что, подумал я, если ты  не  можешь  сделать  ничего,  что
доставило бы  тебе  радость,  сделай  что-нибудь  подлое.  Масса  способов
наслаждаться  подлостью.  Почему?  Кто   знает?   Нужно   лишь   придумать
какую-нибудь гадость и потом плакать, сожалеть о содеянном. Так по крайней
мере чувствуешь, что сделал что-то до  конца.  Так  что  я  решил  сделать
что-нибудь гадкое...
   - Поздравляю, ты преуспел!
   Бутс  уставился  на  свои  руки,  как  будто  они  держали  старое,  но
испытанное оружие.
   - Вы когда-нибудь убивали черепаху?
   - Что?..
   - Когда мне было десять лет, я впервые задумался о смерти.  Я  подумал,
что черепаха,  эта  большая,  бессловесная,  похожая  на  булыжник  тварь,
собирается еще жить и жить долго после того, как я умру. И  я  решил,  что
если я должен уйти, пусть черепаха уйдет первой. Поэтому я взял  кирпич  и
бил ее по спине до тех пор, пока панцирь не треснул и она не сдохла.
   Байес замедлил шаги. Бутс сказал:
   - По той же причине я однажды отпустил бабочку. Она села мне на руку. Я
вполне мог раздавить ее. Но я не сделал этого. Не сделал потому, что знал:
через десять минут или через час какая-нибудь птица поймает  и  съест  ее.
Поэтому я дал ей улететь. Но черепахи?! Они валяются на задворках и  живут
вечно. Поэтому я взял кирпич - и я жалел  об  этом  многие  месяцы.  Может
быть, и сейчас еще жалею. Смотрите...
   Его руки дрожали.
   - Ладно, - сказал Байес.  -  Но  какое,  черт  возьми,  все  это  имеет
отношение к тому, что ты оказался сегодня здесь?
   - Что? Как какое отношение?! - закричал Бутс, глядя на него, как  будто
это ОН, Байес, сошел с ума. - Вы что, не слушали? Отношение!.. Бог мой,  я
ревнив! Ревнив ко всему! Ревнив  ко  всему,  что  работает  правильно,  ко
всему, что совершенно, ко всему, что прекрасно само по себе, ко всему, что
живет и будет жить вечно, мне безразлично что! Ревнив!
   - Но ты же не можешь ревновать к машинам.
   - Почему нет, черт  побери!  -  Бутс  схватился  за  спинку  сиденья  и
медленно подался вперед, уставившись на осевшую фигуру  в  высоком  кресле
там, посреди сцены. - Разве в девяноста девяти случаях из  ста  машины  не
являются более совершенными, чем большинство людей, которых вы  когда-либо
знали? Разве они не делают правильно и точно то, что им  положено  делать?
Сколько людей вы знаете, которые правильно  и  точно  делают  то,  что  им
положено делать, хотя бы наполовину, хотя бы на одну  треть?  Эта  чертова
штука там, на сцене, эта машина не только выглядит совершенно, она говорит
и работает, как само совершенство. Больше того, если ее смазывать, вовремя
заводить и изредка регулировать,  она  будет  точно  так  же  говорить,  и
двигаться, и выглядеть великой и прекрасной через сто,  через  двести  лет
после того, как я давно уже сгнию в могиле. Ревнив?  Да,  черт  возьми,  я
ревнив!
   - Но машина НЕ ЗНАЕТ этого...
   - Я знаю. Я чувствую! - сказал  Бутс.  -  Я,  посторонний  наблюдатель,
смотрю на творение. Я всегда за бортом. Никогда не был  при  деле.  Машина
творит. Я нет. Ее построили, чтобы она правильно и точно  делала  одну-две
операции. И сколько бы я ни учился, сколько бы я ни старался до конца дней
своих делать  что-нибудь  -  неважно  что,  -  никогда  я  не  буду  столь
совершенен, столь, прекрасен, столь гарантирован от  разрушения,  как  эта
штука там, этот человек, эта машина, это создание, этот президент...
   Теперь он стоял и кричал на сцену через весь зал.
   А Линкольн молчал. Машинное масло капля за каплей медленно собиралось в
блестящую лужу на полу под креслом.
   - Этот президент, - заговорил снова Бутс,  как  будто  до  него  только
сейчас дошел смысл случившегося. - Этот президент. Да, Линкольн. Разве  вы
не видите? Он умер давным-давно. Он не может  быть  живым.  Он  просто  не
может быть живым. Это неправильна. Сто лет тому назад - и  вот  он  здесь.
Его убили, похоронили, а  он  все  равно  живет,  живет,  живет.  Сегодня,
завтра, послезавтра - всегда. Так что его зовут Линкольн, а меня Бутс... Я
просто должен был прийти...
   Он затих, уставившись в пространство.
   - Сядь, - тихо сказал Байес.
   Бутс сел, и Байес кивнул охраннику:
   - Подождите снаружи, пожалуйста.
   Когда охранник вышел и в  зале  остались  только  он,  и  Бутс,  и  эта
неподвижная фигура, там, в кресле, Байес медленно повернулся и пристально,
в упор посмотрел на убийцу. Тщательно взвешивая каждое слово, он сказал:
   - Хорошо, но это не все.
   - Что?
   - Ты не все сказал, почему ты сегодня пришел сюда.
   - Я все сказал.
   - Это тебе только кажется, что ты все сказал. Ты обманываешь сам  себя.
Но все это в конечном итоге сводится к одному.  К  одной  простой  истине.
Скажи, тебе очень хочется увидеть свое фото в газетах, не так ли?
   Бутс промолчал, лишь плечи его слегка выпрямились.
   - Хочешь, чтобы твою физиономию разглядывали на журнальных обложках  от
Нью-Йорка до Сан-Франциско?
   - Нет.
   - Выступать по телевидению?
   - Нет.
   - Давать интервью по радио?
   - Нет!
   - Хочешь быть героем шумных судебных процессов? Чтобы  юристы  спорили,
можно ли судить человека за новый вид убийства...
   - Нет!
   - ...то есть за убийство человекоподобной машины?..
   - Нет!
   Байес остановился. Теперь Бутс дышал часто: вдох-выдох, вдох-выдох. Его
глаза бешено бегали по сторонам. Байес продолжал:
   - Здорово, не правда ли: двести миллионов человек будут говорить о тебе
завтра, послезавтра, на следующей неделе, через год!
   Молчание.
   - Продать свои мемуары международным синдикатам за кругленькую сумму?
   Пот стекал по лицу Бутса и каплями падал на ладони.
   - Хочешь, я отвечу на все эти вопросы, а?
   Байес помолчал. Бутс ждал новых вопросов, нового напора.
   - Ладно, - сказал Байес. - Ответ на все эти вопросы...
   Кто-то постучал в дверь.
   Байес вздрогнул.
   Стук повторился, на этот раз настойчивей и громче.
   - Байес! Это я, Фиппс! Открой мне дверь!
   Стук, дерганье, потом тишина.
   Байес и Бутс смотрели друг на друга, как заговорщики.
   - Открой дверь! Ради бога, открой мне дверь!
   Снова бешеный барабанный грохот, потом опять тишина.  Там,  за  дверью,
Фиппс дышал часто и тяжело. Его шаги отдалились, потом  стихли.  Наверное,
он побежал искать другой вход.
   - На чем я остановился? - спросил Байес. - Ах, да.  Ответ  на  все  мои
вопросы.    Скажи,    тебе    ужасно    хочется    приобрести    всемирную
телекинорадиожурнальногазетную известность?
   Бутс раскрыл рот, но промолчал.
   - Н-Е-Т, - раздельно, по буквам произнес Байес.
   Он протянул руку, достал из внутреннего кармана бумажник Бутса, вытащил
из него все документы и положил пустой Бумажник обратно.
   - Нет? - ошеломленно спросил Бутс.
   - Нет, мистер Бутс.  Не  будет  фотографий.  Не  будет  телепередач  от
Нью-Йорка до Сан-Франциско. Не будет журналов. Не будет статей в  газетах.
Не будет рекламы. Не будет славы. Не будет почета. Веселья. Самосожаления.
Покорности судьбе. Бессмертия. Абсурдных рассуждении  о  власти  автоматов
над людьми.  Великомученичества.  Временного  возвышения  над  собственной
посредственностью. Сладостных страданий.  Сентиментальных  слез.  Судебных
процессов. Адвокатов. Биографов, превозносящих вас до небес  через  месяц,
год, тридцать лет, шестьдесят лет, девяносто лет.  Двусмысленных  сплетен.
Денег. Не будет. Нет.
   Бутс поднимался над креслом, как будто его вытягивали  на  веревке:  он
был смертельно бледен, словно невидимой рукой его умыли белилами.
   - Я не понимаю. Я...
   - Вы заварили всю эту кашу? Да. Но ставка ваша бита. И я  испорчу  ваше
представление. Потому что теперь, мистер Бутс, когда  все  уже  сказано  и
сделано, когда все аргументы исчерпаны и все итоги подведены, вы просто не
существующее и никогда не  существовавшее  ничтожество.  И  таковым  вы  и
останетесь: маленьким и посредственным, подленьким, дрянным  и  трусливым.
Вы коротышка, Бутс, и я буду мять, давить, сжимать, дубасить вас, пока  вы
не станете еще на дюйм короче, вместо того чтобы помогать вам  возвыситься
и упиваться своим трехметровым ростом.
   - Вы не посмеете! - взвизгнул Бутс.
   - О нет, мистер Бутс, - тотчас ответил Байес почти счастливым  голосом.
- Я посмею. Я могу сделать с вами все, что  захочу.  Больше  того,  мистер
Бутс, ничего этого никогда не было.
   Стук возобновился. Теперь стучали в запертую дверь за кулисами.
   - Байес, ради бога, откройте мне дверь! Это Фиппс! Байес! Байес!
   Очень спокойно, с великолепным самообладанием Байес ответил:
   - Одну минуту.
   Он знал, что через несколько минут все взорвется и забурлит, от  тишины
и  спокойствия  не  останется  и  следа,  но   сейчас   пока   было   это:
величественная безмятежная игра, и он в заглавной роли; он должен доиграть
ее до конца. Он обращался к убийце и смотрел, как тот ерзает в кресле;  он
снова говорил и смотрел, как тот съеживается:
   - Ничего и никогда этого не было, мистер Бутс.  Вы  можете  кричать  на
каждом углу - мы будем отрицать это. Вы никогда здесь  не  были.  Не  было
пистолета. Не было выстрела. Не было электронно-счетного убийства. Не было
осквернения. Не было шока. Паники. Толпы. Что с вами? Посмотрите  на  свое
лицо. Почему у вас подкашиваются ноги?  Почему  вы  садитесь?  Почему  вас
трясет? Вы разочарованы? Я нарушил ваши планы?  Хорошо!  -  Он  кивнул  на
выход. - А теперь, мистер Бутс, убирайтесь отсюда вон.
   - Вы не имеете...
   Байес мягко шагнул вперед, взял Бутса за галстук  и  медленно  поставил
убийцу на ноги. Теперь Бутс вплотную чувствовал его дыхание.
   - Если вы когда-нибудь расскажете своей жене, приятелю,  начальнику  по
службе,  мужчине,   женщине,   дяде,   тете,   троюродному   брату,   если
когда-нибудь, ложась в постель, вы самому себе начнете рассказывать  вслух
об этой пакости, которую вы натворили, - знаете,  что  я  с  вами  сделаю,
мистер Бутс? Я не скажу вам этого, я не могу сейчас сказать. Но это  будет
ужасно...
   Бутс был бледен, его трясло.
   - Что я сказал сейчас, мистер Бутс?
   - Вы убьете меня?
   - Повтори снова!
   Он тряс Бутса до тех пор,  пока  слова  не  сорвались  между  стучащими
зубами: "...убьете меня!.."
   Байес держал его  крепко,  и  тряс,  и  тряс  долго  и  безостановочно,
чувствуя, как паника охватывает Бутса.
   - Прощайте, Господин Никто.  Не  будет  статей  в  журналах,  не  будет
веселья,  не  будет  телевидения,  не  будет  славы,  не  будет   аршинных
заголовков. А теперь убирайтесь отсюда вон,  бегите,  бегите,  пока  я  не
прибил вас.
   Он  подтолкнул  Бутса.  Бутс  побежал,  споткнулся,  встал  и  неуклюже
поскакал к двери, которая а тот же момент затряслась и загрохотала.
   Фиппс был там, Фиппс взывал из темноты.
   - В другую дверь, - сказал Байес.
   Он указал пальцем, и Бутс, развернувшись как волчок, помчался  в  новом
направлении.
   - Стой, - сказал Байес.
   Он пересек зал, подошел к Бутсу, поднял руку и изо всех сил влепил  ему
звонкую пощечину. Пот маленькими каплями брызнул у него из-под руки.
   - Я должен был сделать это, - сказал Байес. - Только раз.
   Он взглянул на руку, потом повернулся и открыл дверь. Оба посмотрели на
таинственный мир ночи с холодными звездами, на тихие  улицы,  где  уже  не
было никакой толпы.
   - Убирайся, - сказал Байес.
   Бутс исчез. Дверь с треском захлопнулась. Байес  прислонился  к  ней  в
изнеможении, тяжело дыша.
   По другую сторону зала снова начался стук, грохот, дерганье двери.  Это
Фиппс. Нет, Фиппс пусть еще подождет. Сейчас...
   Театр казался огромным и пустынным, как поле Геттисберга,  когда  толпа
разбрелась по домам и солнце уже зашло; где была толпа и где ее больше  не
было; где отец поднял вверх ребенка и где мальчик повторял слова, но  слов
уже тоже не было...
   Он поднялся на сцену  и  протянул  руку.  Его  пальцы  коснулись  плеча
Линкольна.
   Слезы текли по лицу Байеса.
   Он плакал. Рыдания душили его. Он не мог остановить их.
   Линкольн был мертв. Линкольн был МЕРТВ.
   А он позволил его убийце уйти.